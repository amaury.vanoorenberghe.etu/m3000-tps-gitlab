# Annuaire de TP S3

## Avant-propos
> Il n'est pas recommandé de juste copier-coller les repos listés ici, les implémentations présentées sont là à titre indicatif et peuvent s'avérer fausses ou incomplètes. Gardez en tête qu'un code passant les tests n'est pas signe d'un code qui fonctionne ou qui sera facile à maintenir!

> Il n'est pas recomendé de juste copier-coller les tps listés ici lors des séances de TP. Essayez de réaliser les TPs par vous-même, ce repo est principalement là si vous n'avez pas fini le TP en temps et que vous rencontrez des difficultés chez vous en le finissant.

## Introduction

Bienvenu sur l'annuaire de TP au S3 (2021-2022) :+1:

Ce projet GitLab liste (par module) une liste de liens vers d'autres répertoires contenant les TP achevés d'autres étudiants.

l'objectif est que les étudiants ayant du mal à terminer les TP puissent consulter ces derniers librement pour comprendre une ou plusieurs manières d'effectuer le TP.

Le répertoire [`annuaire/`](annuaire/) contient les différentes matières

## Contribuer
Si vous souhaitez ajouter un repo git à la liste, n'hésitez pas à ouvrir une [Demande de fusion (Pull/Merge Request)](https://gitlab.univ-lille.fr/amaury.vanoorenberghe.etu/m3000-tps-gitlab/-/merge_requests), ou à ouvrir un [Ticket (Issue)](https://gitlab.univ-lille.fr/amaury.vanoorenberghe.etu/m3000-tps-gitlab/-/issues)

***Je vais essayer de checker les contributions entrantes du mieux possible, il se peut que certaines contributions prennent un peu plus de temps à êtres traitées en fonction de mon emploi du temps***